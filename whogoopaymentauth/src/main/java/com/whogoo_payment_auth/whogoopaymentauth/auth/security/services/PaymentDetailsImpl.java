package com.whogoo_payment_auth.whogoopaymentauth.auth.security.services;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.whogoo_payment_auth.whogoopaymentauth.auth.models.Payment;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


public class PaymentDetailsImpl implements UserDetails{

    private static final long serialVersionUID = 1L;

    private Long id;

    private String username;

    private String email;


    private String nama_rekening;


    private String bank;

    @JsonIgnore
    private String verif_code;

    private Collection<? extends GrantedAuthority> authorities;

    public PaymentDetailsImpl(Long id, String username, String email, String nama_rekening, String bank,String verif_code,
                              Collection<? extends GrantedAuthority> authorities) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.nama_rekening=nama_rekening;
        this.bank=bank;
        this.verif_code=verif_code;
        this.authorities = authorities;
    }

    public static PaymentDetailsImpl build(Payment payment) {
        List<GrantedAuthority> authorities = payment.getRoles().stream()
                .map(role -> new SimpleGrantedAuthority(role.getName().name()))
                .collect(Collectors.toList());

        return new PaymentDetailsImpl(
                payment.getId(),
                payment.getUsername(),
                payment.getEmail(),
                payment.getNama_rekening(),
                payment.getBank(),
                payment.getVerif_code(),
                authorities);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return verif_code;
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }


    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        PaymentDetailsImpl user = (PaymentDetailsImpl) o;
        return Objects.equals(id, user.id);
    }

    public String getNama_rekening() {
        return nama_rekening;
    }

    public void setNama_rekening(String nama_rekening) {
        this.nama_rekening = nama_rekening;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getVerif_code() {
        return verif_code;
    }

    public void setVerif_code(String verif_code) {
        this.verif_code = verif_code;
    }
}
