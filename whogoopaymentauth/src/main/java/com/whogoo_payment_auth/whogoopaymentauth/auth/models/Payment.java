package com.whogoo_payment_auth.whogoopaymentauth.auth.models;


import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(	name = "payments",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "username"),
                @UniqueConstraint(columnNames = "email")
        })
public class Payment extends AuditModel {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long id;

        @NotBlank
        @Size(max = 20)
        private String username;

        @NotBlank
        @Size(max = 50)
        @Email
        private String email;


        @NotBlank
        @Size(max = 50)
        private String nama_rekening;

        @NotBlank
        @Size(max = 50)
        private String bank;


        @NotBlank
        @Size(max = 120)
        private String verif_code;

        @ManyToMany(fetch = FetchType.LAZY)
        @JoinTable(	name = "payment_roles",
                joinColumns = @JoinColumn(name = "payment_id"),
                inverseJoinColumns = @JoinColumn(name = "role_id"))
        private Set<Role> roles = new HashSet<>();

        public Payment() {
        }

        public Payment(String username, String email, String nama_rekening, String bank, String verif_code) {
                this.username = username;
                this.email = email;
                this.nama_rekening=nama_rekening;
                this.bank=bank;
                this.verif_code = verif_code;
        }

        public Long getId() {
                return id;
        }

        public void setId(Long id) {
                this.id = id;
        }

        public String getUsername() {
                return username;
        }

        public void setUsername(String username) {
                this.username = username;
        }

        public String getEmail() {
                return email;
        }

        public void setEmail(String email) {
                this.email = email;
        }


        public String getBank() {
                return bank;
        }

        public void setBank(String bank) {
                this.bank = bank;
        }

        public String getNama_rekening() {
                return nama_rekening;
        }

        public void setNama_rekening(String nama_rekening) {
                this.nama_rekening = nama_rekening;
        }

        public String getVerif_code() {
                return verif_code;
        }

        public void setVerif_code(String verif_code) {
                this.verif_code = verif_code;
        }

        public Set<Role> getRoles() {
                return roles;
        }

        public void setRoles(Set<Role> roles) {
                this.roles = roles;
        }
}
