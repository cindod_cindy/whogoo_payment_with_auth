package com.whogoo_payment_auth.whogoopaymentauth.auth.security.services;

import com.whogoo_payment_auth.whogoopaymentauth.auth.models.Payment;
import com.whogoo_payment_auth.whogoopaymentauth.auth.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PaymentDetailsServiceImpl implements UserDetailsService{

    @Autowired
    PaymentRepository paymentRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Payment payment = paymentRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));

        return PaymentDetailsImpl.build(payment);
    }

}
