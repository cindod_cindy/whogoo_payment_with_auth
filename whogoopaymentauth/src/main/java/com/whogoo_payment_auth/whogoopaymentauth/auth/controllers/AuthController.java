package com.whogoo_payment_auth.whogoopaymentauth.auth.controllers;

import com.whogoo_payment_auth.whogoopaymentauth.auth.models.ERole;
import com.whogoo_payment_auth.whogoopaymentauth.auth.models.Payment;
import com.whogoo_payment_auth.whogoopaymentauth.auth.models.Role;
import com.whogoo_payment_auth.whogoopaymentauth.auth.payloads.request.LoginRequest;
import com.whogoo_payment_auth.whogoopaymentauth.auth.payloads.request.SignupRequest;
import com.whogoo_payment_auth.whogoopaymentauth.auth.payloads.request.UserGetPaymentDataRequest;
import com.whogoo_payment_auth.whogoopaymentauth.auth.payloads.response.JwtResponse;
import com.whogoo_payment_auth.whogoopaymentauth.auth.payloads.response.MessageResponse;
import com.whogoo_payment_auth.whogoopaymentauth.auth.repository.RoleRepository;
import com.whogoo_payment_auth.whogoopaymentauth.auth.repository.PaymentRepository;
import com.whogoo_payment_auth.whogoopaymentauth.auth.security.jwt.JwtUtils;
import com.whogoo_payment_auth.whogoopaymentauth.auth.security.services.PaymentDetailsImpl;
import com.whogoo_payment_auth.whogoopaymentauth.auth.security.services.UserGetPaymentDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


//@CrossOrigin(origins = "*", maxAge = 3600)
@CrossOrigin("http:// 192.168.1.90:8088")
@RestController
@RequestMapping("/whogooapp/api/auth/payment")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    PaymentRepository paymentRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    UserGetPaymentDataService userGetPaymentDataService;

    @PostMapping("/roles")
    public void insertkarywan(@RequestBody Role rolereq){
        Role role= new Role();
        role.setName(rolereq.getName());
        roleRepository.save(role);
    }

    @GetMapping("/allUser")
    public List<Payment> getallPayment(){
        return paymentRepository.findAll();
    }

    @PostMapping("/login-payment")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getVerif_code()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        PaymentDetailsImpl paymentDetails = (PaymentDetailsImpl) authentication.getPrincipal();
        List<String> roles = paymentDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        return ResponseEntity.ok(new JwtResponse(jwt,
                paymentDetails.getId(),
                paymentDetails.getUsername(),
                paymentDetails.getEmail(),
                paymentDetails.getNama_rekening(),
                paymentDetails.getBank(),
                paymentDetails.getVerif_code(),
                roles));
    }

    @PostMapping("/regis-payment")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        if (paymentRepository.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: nama sudah pernah digunakan"));
        }

        if (paymentRepository.existsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email sudah pernah digunakan"));
        }

        // Create new user's account
        Payment payment = new Payment(signUpRequest.getUsername(),
                signUpRequest.getEmail(),signUpRequest.getNama_rekening(),signUpRequest.getBank(),
                encoder.encode(signUpRequest.getVerif_code()));

        Set<String> strRoles = signUpRequest.getRole();
        Set<Role> roles = new HashSet<>();

        if (strRoles == null) {
            Role userRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "admin":
                        Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(adminRole);

                        break;
                    case "seller":
                        Role modRole = roleRepository.findByName(ERole.ROLE_SELLER)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(modRole);

                        break;
                    default:
                        Role userRole = roleRepository.findByName(ERole.ROLE_BUYER)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(userRole);
                }
            });
        }

        payment.setRoles(roles);
        paymentRepository.save(payment);

        return ResponseEntity.ok(new MessageResponse("Daftar Sukses"));
    }

    @PostMapping("/get-by-email")
    public ResponseEntity<Payment> getPaymentUserByEmail(@Valid @RequestBody UserGetPaymentDataRequest userGetPaymentDataRequest) {
        return userGetPaymentDataService.getPaymentByEmail(userGetPaymentDataRequest);
    }
}
