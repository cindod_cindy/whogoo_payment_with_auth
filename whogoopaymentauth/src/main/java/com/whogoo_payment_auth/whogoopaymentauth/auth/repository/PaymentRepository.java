package com.whogoo_payment_auth.whogoopaymentauth.auth.repository;

import com.whogoo_payment_auth.whogoopaymentauth.auth.models.Payment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long>{

    Optional<Payment> findByUsername(String username);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);

    Optional<Payment> findByEmail(String email);
}
