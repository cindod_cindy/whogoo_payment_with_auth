package com.whogoo_payment_auth.whogoopaymentauth.auth.models;

public enum ERole {

    ROLE_SELLER,
    ROLE_BUYER,
    ROLE_ADMIN
}
