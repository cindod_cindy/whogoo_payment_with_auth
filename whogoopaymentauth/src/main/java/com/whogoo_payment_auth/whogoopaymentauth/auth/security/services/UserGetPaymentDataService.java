package com.whogoo_payment_auth.whogoopaymentauth.auth.security.services;

import com.whogoo_payment_auth.whogoopaymentauth.auth.models.Payment;
import com.whogoo_payment_auth.whogoopaymentauth.auth.payloads.request.UserGetPaymentDataRequest;
import com.whogoo_payment_auth.whogoopaymentauth.auth.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserGetPaymentDataService {

    @Autowired
    PaymentRepository paymentRepository;


    public ResponseEntity<Payment> getPaymentByEmail(UserGetPaymentDataRequest userGetPaymentDataRequest) {
        String email = userGetPaymentDataRequest.getEmail();
        Optional<Payment> paymentData = paymentRepository.findByEmail(email);
        if (paymentData.isPresent()) {
            return new ResponseEntity<>(paymentData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
