package com.whogoo_payment_auth.whogoopaymentauth.auth.repository;

import com.whogoo_payment_auth.whogoopaymentauth.auth.models.ERole;
import com.whogoo_payment_auth.whogoopaymentauth.auth.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long>{
    Optional<Role> findByName(ERole name);
}
