package com.whogoo_payment_auth.whogoopaymentauth.auth.payloads.request;

import javax.validation.constraints.NotBlank;

public class UserGetPaymentDataRequest {

    @NotBlank
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
