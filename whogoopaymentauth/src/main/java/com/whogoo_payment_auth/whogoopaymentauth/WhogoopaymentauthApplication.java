package com.whogoo_payment_auth.whogoopaymentauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class WhogoopaymentauthApplication {

	public static void main(String[] args) {
		SpringApplication.run(WhogoopaymentauthApplication.class, args);
	}

}
